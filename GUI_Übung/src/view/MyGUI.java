package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtEingabefeld;
	JLabel lblText;
	Color farbe = Color.WHITE;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 437, 596);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblText = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		lblText.setBounds(0, 38, 419, 16);
		contentPane.add(lblText);
		
		//Hintergrund Rosa
		JButton btnRosa = new JButton("Rosa");
		btnRosa.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		btnRosa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRosa_clicked();
			}
		});
		btnRosa.setBounds(12, 96, 112, 25);
		contentPane.add(btnRosa);
		
		//Hintergrund Blau
		JButton btnBlau = new JButton("Blau");
		btnBlau.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBlue_clicked();
			}
		});
		btnBlau.setBounds(151, 96, 112, 25);
		contentPane.add(btnBlau);
		
		
		//Hintergrund Grün
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGrn_clicked();
			}
		});
		btnGrn.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		btnGrn.setBounds(290, 96, 112, 25);
		contentPane.add(btnGrn);
		
		
		//Hintergrund Gelb
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGelb_clicked();
			}
		});
		btnGelb.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		btnGelb.setBounds(12, 130, 112, 25);
		contentPane.add(btnGelb);
		
		
		//Hintergrund Standard
		JButton btnStandard = new JButton("Standard");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStandard_clicked();
			}
		});
		btnStandard.setFont(new Font("Segoe UI", Font.PLAIN, 13));
		btnStandard.setBounds(151, 130, 112, 25);
		contentPane.add(btnStandard);
		
		
		//Hintergrund Farbe wählen
		JButton btnFarbeWählen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWählen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color farbe = JColorChooser.showDialog(null, "Farbauswahl", null);
				getContentPane().setBackground(farbe);
			}
		});
		btnFarbeWählen.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		btnFarbeWählen.setBounds(290, 130, 112, 25);
		contentPane.add(btnFarbeWählen);
		
		
		JLabel lblAufgabe1_Hintergrund = new JLabel("Aufgabe 1: Hintergrund \u00E4ndern");
		lblAufgabe1_Hintergrund.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe1_Hintergrund.setBounds(12, 74, 221, 16);
		contentPane.add(lblAufgabe1_Hintergrund);
		
		
		JLabel lblAufgabe2_Textformat = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2_Textformat.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe2_Textformat.setBounds(12, 172, 187, 16);
		contentPane.add(lblAufgabe2_Textformat);
		
		//Arial
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnArial_clicked();
			}
		});
		btnArial.setBounds(12, 192, 112, 25);
		contentPane.add(btnArial);
		
		
		// Comic Sans
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnComicSansMS_clicked();
			}
		});
		btnComicSansMs.setFont(new Font("Segoe UI", Font.PLAIN, 10));
		btnComicSansMs.setBounds(151, 193, 112, 25);
		contentPane.add(btnComicSansMs);
		
		
		// Courier New
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCourierNew_clicked();
			}
		});
		btnCourierNew.setBounds(290, 192, 112, 25);
		contentPane.add(btnCourierNew);
		
		txtEingabefeld = new JTextField();
		txtEingabefeld.setText("Hier bitte Text eingeben");
		txtEingabefeld.setBounds(12, 230, 390, 22);
		contentPane.add(txtEingabefeld);
		txtEingabefeld.setColumns(10);
		
		JButton btnLabelSchreiben = new JButton("Ins Label schreiben");
		btnLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLabelSchreiben_clicked();
			}
		});
		btnLabelSchreiben.setBounds(12, 265, 185, 25);
		contentPane.add(btnLabelSchreiben);
		
		JButton btnTextLöschen = new JButton("Text im Label l\u00F6schen");
		btnTextLöschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTextLöschen_clicked();
			}
		});
		btnTextLöschen.setBounds(217, 265, 185, 25);
		contentPane.add(btnTextLöschen);
		
		JLabel lblAufgabe3_Schriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3_Schriftfarbe.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe3_Schriftfarbe.setBounds(12, 303, 221, 16);
		contentPane.add(lblAufgabe3_Schriftfarbe);
		
		
		//Rosa Schrift
		JButton btnRosa_Schrift = new JButton("Rosa");
		btnRosa_Schrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRosa_Schrift_clicked();
			}
		});
		btnRosa_Schrift.setBounds(12, 325, 112, 25);
		contentPane.add(btnRosa_Schrift);
		
		
		//Blaue Schrift
		JButton btnBlau_Schrift = new JButton("Blau");
		btnBlau_Schrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBlau_Schrift_clicked();
			}
		});
		btnBlau_Schrift.setBounds(151, 325, 112, 25);
		contentPane.add(btnBlau_Schrift);
		
		
		
		JButton btnSchwarz_Schrift = new JButton("Schwarz");
		btnSchwarz_Schrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSchwarz_Schrift_clicked();
			}
		});
		btnSchwarz_Schrift.setBounds(290, 325, 112, 25);
		contentPane.add(btnSchwarz_Schrift);
		
		JLabel lblAufgabe4_Schriftgröße = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4_Schriftgröße.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe4_Schriftgröße.setBounds(12, 363, 251, 16);
		contentPane.add(lblAufgabe4_Schriftgröße);
		
		JButton btnVergrößern = new JButton("+");
		btnVergrößern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnVergrößern_clicked();
			}
		});
		btnVergrößern.setBounds(12, 385, 187, 25);
		contentPane.add(btnVergrößern);
		
		JButton btnVerkleinern = new JButton("-");
		btnVerkleinern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnVerkleinern_clicked();
			}
		});
		btnVerkleinern.setBounds(217, 385, 185, 25);
		contentPane.add(btnVerkleinern);
		
		JLabel lblAufgabe5_Textausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5_Textausrichtung.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe5_Textausrichtung.setBounds(12, 423, 221, 16);
		contentPane.add(lblAufgabe5_Textausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLinksbündig_clicked();
			}
		});
		btnLinksbndig.setBounds(12, 446, 112, 25);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnZentriert_clicked();
			}
		});
		btnZentriert.setBounds(151, 446, 112, 25);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRechtsbündig_clicked();
			}
		});
		btnRechtsbndig.setBounds(290, 446, 112, 25);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabe6_beenden = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6_beenden.setFont(new Font("Segoe UI", Font.BOLD, 13));
		lblAufgabe6_beenden.setBounds(12, 484, 285, 16);
		contentPane.add(lblAufgabe6_beenden);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnExit_clicked();
			}
		});
		btnExit.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		btnExit.setBounds(12, 505, 390, 40);
		contentPane.add(btnExit);
	}

	public void btnRosa_clicked() {
		this.contentPane.setBackground(Color.pink);	
	}
	
	public void btnBlue_clicked() {
		this.contentPane.setBackground(new Color(0xADD8e6));	
	}
	
	public void btnGrn_clicked() {
		this.contentPane.setBackground(new Color(0x8FBC8F));	
	}
	
	public void btnGelb_clicked() {
		this.contentPane.setBackground(new Color(0xFFE4B5));	
	}
	
	public void btnStandard_clicked() {
		this.contentPane.setBackground(new Color(0xEEEEEE));	
	}
	
	public void btnArial_clicked() {
		this.lblText.setFont(new Font("Arial", Font.PLAIN, 12));	
	}
	
	public void btnComicSansMS_clicked() {
		this.lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
	}
	
	public void btnCourierNew_clicked() {
		this.lblText.setFont(new Font("Courier New", Font.PLAIN, 12));
	}
	
	public void btnRosa_Schrift_clicked() {
		this.lblText.setForeground(Color.pink);
	}
	
	public void btnBlau_Schrift_clicked() {
		this.lblText.setForeground(Color.blue);
	}
	
	public void btnSchwarz_Schrift_clicked() {
		this.lblText.setForeground(Color.black);
	}
	
	public void btnLabelSchreiben_clicked() {
		lblText.setText(txtEingabefeld.getText());
	}
	
	public void btnTextLöschen_clicked() {
		this.lblText.setText("");
	}
	
	public void btnVergrößern_clicked() {
		int groesse = lblText.getFont().getSize();
		lblText.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
	}
	
	public void btnVerkleinern_clicked() {
		int groesse = lblText.getFont().getSize();
		lblText.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
	}
	
	public void btnLinksbündig_clicked() {
		this.lblText.setHorizontalAlignment(SwingConstants.LEFT);
	}
	
	public void btnZentriert_clicked() {
		this.lblText.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	public void btnRechtsbündig_clicked() {
	this.lblText.setHorizontalAlignment(SwingConstants.RIGHT);
	}
	
	public void btnExit_clicked() {
		System.exit(1);
		}
	
}
